<form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get">
    <label for="search">Найти подходящий кредит для Вас</label>
    <input type="text" name="s" id="search" value="<?php the_search_query(); ?>" />
    <input type="submit" id="searchsubmit" value="найти" class="btn" />
</form>