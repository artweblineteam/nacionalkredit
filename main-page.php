<?php
/*
Template Name: Home page
*/

get_header(); ?>
<!-- <style type="text/css">
	div.demo { padding: 10px !important; width: 200px;}
	.ui-widget{font-size: 0.6em !important;}
	</style> -->
	<!-- #functional-section -->
	<section id="functional-section">
		<div class="container">
			<div class="row">
				<div class="col-md-8">

					<div class="calc-title">
						<h1>ВАМ СРОЧНО НУЖНЫ ДЕНЬГИ?</h1>
						<span class="h1-subtitle">ОЦЕНИТЕ ВАШИ ВОЗМОЖНОСТИ НА ПОЛУЧЕНИЕ КРЕДИТА</span>
					</div>
					<a href="/kreditnyi-kalkulyator" class="btn mobile-view btn-mobile-view">Кредитный калькулятор</a>
					<div class="calc">
						<?php get_template_part( 'template-parts/home', 'calc' );?>
						
					</div>
				</div>
				<div class="col-md-4">
					<div class="why-us">
						<h4 class="icon-hand"><span>ПОЧЕМУ СТОИТ ВЫБРАТЬ НАС?</span></h4>

						<div class="why-us-item first">
							<img src="<?php bloginfo('template_directory'); ?>/assets/img/icon-1.png" alt="icon-number">
							<div class="why-us-description">
								<h6>Мы не позволим Вам необоснованно переплатить</h6>
								<span>При оформлении документов всегда нужно смотреть на сумму. Мы покажем ее до момента подписания договора.</span>
							</div>
						</div>

						<div class="why-us-item second">
							<img src="<?php bloginfo('template_directory'); ?>/assets/img/icon-2.png" alt="icon-number">
							<div class="why-us-description">
								<h6>Мы предоставим Вам выгодные условия</h6>
								<span>Брать деньги под залог лучше на выгодных условиях. Наши специалисты помогут выбрать Вам подходящий для Вас вариант.</span>
							</div>
						</div>

						<div class="why-us-item third">
							<img src="<?php bloginfo('template_directory'); ?>/assets/img/icon-3.png">
							<div class="why-us-description" alt="icon-number">
								<h6>От нас исходят лучшие предложения</h6>
								<span>Наши специалисты помогут взять залог под недвижимость с минимальной переплатой, на выгодный срок и с удобной суммой ежемесячного платежа.</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- #functional-section END -->

	<!-- #process -->
	<section id="process">
		<div class="container">
			<div class="row">
				<h3>ПРОЦЕСС ПОЛУЧЕНИЯ КРЕДИТА</h3>
				<div class="col-md-1"></div>
				<div class="col-md-2">
					<div class="process-item">
						<img src="<?php bloginfo('template_directory'); ?>/assets/img/icon-weight.png" alt="icon-weight">
						<span>Оцениваем стоимость недвижимости</span>
					</div>
				</div>
				<div class="col-md-2">
					<div class="process-item">
						<img src="<?php bloginfo('template_directory'); ?>/assets/img/icon-hand-circle.png" alt="icon-hands-circle">
						<span>Согласовываем условия кредитования</span>
					</div>
				</div>
				<div class="col-md-2">
					<div class="process-item">
						<img src="<?php bloginfo('template_directory'); ?>/assets/img/icon-people.png" alt="icon-people">
						<span>Рассчитываем сроки погашения</span>
					</div>
				</div>
				<div class="col-md-2">
					<div class="process-item">
						<img src="<?php bloginfo('template_directory'); ?>/assets/img/icon-list.png" alt="icon-list">
						<span>Заключаем с вами договор</span>
					</div>
				</div>
				<div class="col-md-2">
					<div class="process-item">
						<img src="<?php bloginfo('template_directory'); ?>/assets/img/icon-money-tree.png" alt="icon-money-tree">
						<span>Вы получаете деньги</span>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- #process END -->

	<!-- #benefits -->
	<section id="benefits">
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<?php get_template_part( 'template-parts/make', 'request' ); ?>
					<img src="<?php bloginfo('template_directory'); ?>/assets/img/woman2.png" class="benefits-woman">
				</div>
				<div class="col-md-7">
					<div class="your-benefit">
						<h4 class="icon-badge"><span>ВАШИ ПРЕИМУЩЕСТВА ПРИ РАБОТЕ С НАМИ</span></h4>
						<div class="your-benefit-item">
							<img src="<?php bloginfo('template_directory'); ?>/assets/img/icon-run.png" alt="icon-run">
							<div class="benefit-description"> <span>Высокая оперативность.</span> Решение принимается максимально быстро – в течение 30-ти минут.</div>
						</div>
						<div class="your-benefit-item">
							<img src="<?php bloginfo('template_directory'); ?>/assets/img/icon-card.png" alt="icon-card">
							<div class="benefit-description"> <span>Минимум требований.</span> Нам не требуется от Вас справка о доходах, а получить заем Вы можете даже в случае плохой кредитной истории.</div>
						</div>
						<div class="your-benefit-item">
							<img src="<?php bloginfo('template_directory'); ?>/assets/img/icon-packages.png" alt="icon-package">
							<div class="benefit-description"> <span>Большие суммы.</span> Мы поможем Вам получить залог до 40 миллионов рублей, причем в зависимости от типа и территориального расположения недвижимости сумма может быть увеличена.</div>
						</div>
						<div class="your-benefit-item">
							<img src="<?php bloginfo('template_directory'); ?>/assets/img/icon-interaction.png" alt="icon-interaction">
							<div class="benefit-description"><span>Выгодные условия.</span> С нашей помощью Вы можете получить деньги под залог сроком до 15-ти лет и 12-30%.</div>
						</div>
						<div class="your-benefit-item">
							<img src="<?php bloginfo('template_directory'); ?>/assets/img/icon-chess.png" alt="icon-chess">
							<div class="benefit-description"><span>Минимальные риски.</span> На время выплаты займа Вы по-прежнему остаетесь собственником.</div>
						</div>
					</div>
					<?php get_template_part( 'template-parts/make', 'requestMobile' ); ?>
				</div>
			</div>
		</div>
	</section>
	<!-- #benefits END -->

	<!-- #package -->
	<section id="package">
		<div class="container">
			<div class="row">
				<h3>МИНИМАЛЬНЫЙ ПАКЕТ ДОКУМЕНТОВ</h3>
				<div class="min-package">
					<ul>
						<li class="col-md-3">
							<img src="<?php bloginfo('template_directory'); ?>/assets/img/doc1.png" alt="documents">
							<small><span>Свидетельство о государственной регистрации права - </span> документ подтверждающий права собственности на недвижимость.</small>
						</li>
						<li class="col-md-3">
							<img src="<?php bloginfo('template_directory'); ?>/assets/img/doc2.png" alt="documents">
							<small><span>Право установка - </span> документ на основании которого человек стал собственником недвижимости. (ДКП, дарение, приватизация, наследство, дду).</small>
						</li>
						<li class="col-md-3">
							<img src="<?php bloginfo('template_directory'); ?>/assets/img/doc4.png" alt="documents">
							<small><span>Паспорт - </span> документ удостоверяющий личность человека, содержащий информацию о персональных данных человека.</small>
						</li>
						<li class="col-md-3">
							<img src="<?php bloginfo('template_directory'); ?>/assets/img/doc3.png" alt="documents">
							<small><span>Выписка из домовой книги - </span> документ в котором указанны данные о прописанных людях в объекте недвижимости. </small>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!-- #package END -->

	<!-- #useful -->
	<section id="useful">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div>
						<h4 class="icon-man"><span>ОТЗЫВЫ КЛИЕНТОВ</span></h4>
						<a href="/otzyvy" class="all-testim extra-link">все отзывы</a>
					</div>
					
					<div class="col-md-6 testimonial-item">
						<img src="<?php bloginfo('template_directory'); ?>/assets/img/testim1.png" alt="testimonial">
						<span class="testimonial-text">Заболел близкий родственник. Необходимы были деньги для лечения за границей. А их не хватало. Пробовала занять у знакомых, но в наше время не так много людей, которые могут легко дать крупную сумму взаймы. Бухгалтер с работы посоветовала взять кредит под залог недвижимого... <a href="/otzyvy">читать далее</a> </span>
						<div class="testimonial-meta">
							<span class="testimonial-name">Анна</span>
							<span class="testimonial-date">29.07.2016</span>
						</div>
					</div>
					<div class="col-md-6 testimonial-item">
						<img src="<?php bloginfo('template_directory'); ?>/assets/img/testim2.png" alt="testimonial">
						<span class="testimonial-text">Замечательная команда! Большое спасибо Дмитрию, Юрию, Михаилу, Юле и Ольге за помощь в получении кредита! Слаженная работа, высокий профессионализм каждого сотрудника компании, адекватные цены. Вся работа прозрачна, психологически комфортна, постоянный контакт, оперативность, поддержка. </span>
						<div class="testimonial-meta">
							<span class="testimonial-name">Валерий</span>
							<span class="testimonial-date">13.09.2016</span>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<h4 class="icon-folders"><span>ПОЛЕЗНАЯ ИНФОРМАЦИЯ</span></h4>
					
					<div class="info">
						<?php if ( is_active_sidebar( 'useful_info' ) ) : ?>
					 
						<div id="true-foot-2" class="">
					 
							<?php dynamic_sidebar( 'useful_info' ); ?>
					 
						</div>
					 
						<?php endif; ?>
					</div>
					
				</div>
			</div>
		</div>
	</section>
	<!-- #useful END -->

	<!-- #special-сonditions -->
		<?php get_template_part( 'template-parts/special', 'conditions' ); ?>

	<!-- #special-сonditions END -->

	<!-- #get-loan -->
	<?php get_template_part( 'template-parts/get', 'loan' ); ?>
	<section id="seo-text">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="seo-text">
						<p>Если человек желает взять кредит наличными под залог квартиры, то куда обращаться? Как можно 
						самостоятельно собрать крупную сумму денег в сжатые сроки? Стоит ли просить деньги у родственников и друзей или можно самостоятельно справиться? Нет никаких гарантий, что у них достаточно финансовых средств. Но если определенная сумма потребовалась прямо сейчас, то, как поступить? Если в тайниках уже закончились деньги, и не приходится полагаться на новые пополнения кошелька, то стоит рассмотреть и другие варианты. К примеру, легко и быстро можно взять деньги в кредит, если сотрудничать с банками или финансовыми организациями.</p>

						<p>Не стесняйтесь, <a href="/kontakty">задавайте нам вопросы</a> или звоните по указанным номерам. Мы всегда рады будем Вам помочь!</p>

						<p>Мы предлагаем кредитование под залог недвижимости на гибких условиях. Готовы выступить как кредитный брокер, оказывая помощь в получении банковского кредита, так и выдать кредит вам из собственных средств. При этом мы готовы предложить выгодные ставки от 14% и суммы до 15 млн. руб.</p>

						<p>Подобный кредит не является обременяющим, так что не нужно думать, что кредит под залог квартиры берут только обреченные . Кредитование под гарантию жилья считается вполне привычным делом, так что вы можете не бояться и просить кредит, если есть необходимость. Миллионы людей во всем миру используют такую возможность как кредит под залог недвижимости, такой вид кредитования позволяет значительно улучшить уровень жизни человека.</p>
					</div>
					<a href="#" class="btn mobile-view mobile-view-our-blog">Наш блог</a>
				</div>
				<div class="col-md-4">
					<div class="our-blog-block">
						<a href="/category/blog"><img src="<?php bloginfo('template_directory'); ?>/assets/img/woman3.png" class="woman-blog"></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- #get-loan END -->

<?php get_footer();?>