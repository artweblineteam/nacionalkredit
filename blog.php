<?php
/*
Template Name: Блог
*/
get_header();
 ?>

<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<section id="archive-posts">
			<div class="container">
				<div class="row">
					

		<?php
		if ( have_posts() ) : ?>

			<div class="entry-header">
				<?php
					
					echo '<h1 class="entry-title"><span>' . single_cat_title( '', false ) . '</span></h1>';

				?>
			</div><!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_format() );

			endwhile;

			the_posts_navigation( array(
	'prev_text'          => 'Еще статьи',
	'next_text'          => 'Назад',
) );

		else :

			//get_template_part( 'template-parts/content', 'none' );

		endif; ?>
						</div>
			</div>
		</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer();
