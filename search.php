<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package nacionalkredit
 */

get_header(); ?>
	<section id="content" class="site-content">
		<div class="container">
			<div class="row">
				<div id="primary" class="content-area">
					<main id="main" class="site-main" role="main">

					<?php
					if ( have_posts() ) : ?>

						<div class="page-header">
							<h1 class="page-title"><?php printf( esc_html__( 'Результаты поиска по запросу: "%s"', 'nacionalkredit' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
						</div><!-- .page-header -->

						<?php
						/* Start the Loop */
						while ( have_posts() ) : the_post();

							/**
							 * Run the loop for the search to output the results.
							 * If you want to overload this in a child theme then include a file
							 * called content-search.php and that will be used instead.
							 */
							get_template_part( 'template-parts/content', 'search' );

						endwhile;

					else :

						get_template_part( 'template-parts/content', 'none' );

					endif; 

						the_posts_pagination( array(
							'prev_text'          => __( 'Предыдущая', 'nk' ),
							'next_text'          => __( 'Следующая', 'nk' ),
							/*'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'nk' ) . ' </span>',*/
						) );
					?>
					</main><!-- #main -->
				</div><!-- #primary -->
			</div>
		</div>
	</section>
<?php get_footer();
