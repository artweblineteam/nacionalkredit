<?php
/*
Template Name: Поиск по сайту
*/
get_header();?>
	
	<section id="search-page">
			<div class="container">
		<div class="row">
			<div class="page-header">
					<h1 class="page-title"><?php esc_html_e( 'Воспользуйтесь поиском по сайту', 'nacionalkredit' ); ?></h1>
				</div><!-- .page-header -->
				<div class="search-page-form"> 
					<p>Введите ключевую фразу или слово:</p>
					<?php get_search_form();?>
				</div>
				<div class="page-content">
					<p><?php esc_html_e( 'Также возможно одна из ссылок ниже Вам будет полезна:', 'nacionalkredit' ); ?></p>

					<?php

						the_widget( 'WP_Widget_Recent_Posts' );

						// Only show the widget if site has multiple categories.
						if ( nacionalkredit_categorized_blog('') ) :
					?>

					<?php
						endif;
						
					?>
							</div>
						</div>
				</div><!-- .page-content -->
	</section>	

 <?php get_footer();