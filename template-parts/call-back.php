<div class="callback-popup modal-box desctop-views" id="callbackPopup" style="display: none;">
	<a href="#" class="js-modal-close close">×</a>
	<div class="callback-form">
		<div class="callback-title">
			<h3>Вы заказываете обратный звонок.</h3>
			<span class="callback-subtitle">ДАННАЯ УСЛУГА БЕСПЛАТНА.</span>
		</div>
		<?php echo do_shortcode( '[contact-form-7 id="666" title="Контактная форма 1"]' ); ?>
		<span class="note">* - Мы рассматриваем вопросы только по кредитованию собственников под залог недвижимости в Москве и Московской области.</span>
	</div>
</div>

<div class="callback-button">
	<a href="#" class="js-open-modal btn btn-call-back" data-modal-id="callbackPopup" >Заказать обратный звонок</a>
</div>