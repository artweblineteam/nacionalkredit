	<!-- #get-loan -->
	<section id="get-loan">
		<div class="container">
			<div class="row">
				<div class="loan">
					<h2>ПОЛУЧИТЕ КРЕДИТ</h2>

					<div class="col-md-3">
						<?php if ( is_active_sidebar( 'nk_get_loan_1' ) ) : ?>
							<div id="getLoan-2" class="">
								<?php dynamic_sidebar( 'nk_get_loan_1' ); ?>
							</div>
						<?php endif; ?>
					</div>
					
					<div class="col-md-2">
						<?php if ( is_active_sidebar( 'nk_get_loan_2' ) ) : ?>
							<div id="getLoan-2" class="">
								<?php dynamic_sidebar( 'nk_get_loan_2' ); ?>
							</div>
						<?php endif; ?>
					</div>
					<div class="col-md-2">
						<?php if ( is_active_sidebar( 'nk_get_loan_3' ) ) : ?>
							<div id="getLoan-2" class="">
								<?php dynamic_sidebar( 'nk_get_loan_3' ); ?>
							</div>
						<?php endif; ?>
					</div>
					<div class="col-md-2">
						<?php if ( is_active_sidebar( 'nk_get_loan_4' ) ) : ?>
							<div id="getLoan-2" class="">
								<?php dynamic_sidebar( 'nk_get_loan_4' ); ?>
							</div>
						<?php endif; ?>
					</div>
					<div class="col-md-3">
						<?php if ( is_active_sidebar( 'nk_get_loan_5' ) ) : ?>
							<div id="getLoan-2" class="">
								<?php dynamic_sidebar( 'nk_get_loan_5' ); ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- #get-loan END -->