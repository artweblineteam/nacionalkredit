<div class="tabs">
    <ul>
        <li>
            <span>Банковский кредит</span>
            <a href="#" class="tooltips" data-toggle="tooltip" data-placement="right" title="Кредит, представляемый банками в денежной форме. Банковский кредит имеет строго целевой и срочный характер.">?</a>
        </li>
        <li>
            <span>Кредит от МФО</span>
            <a href="#" class="tooltips" data-toggle="tooltip" data-placement="right" title="Заключается в выдаче небольших кредитов людям, которые не имеют доступа к традиционному банкингу в силу разных причин.">?</a>
        </li>
    </ul>
    <div class="tabs-content">
        <div>
        	<div class="range-slider">
        		<label class="label-home-calc">Сумма кредита</label>
        		<input type="text" class="js-range-slider-bank-1" value="" />
        	</div>
        	<div class="range-slider range-slider-year">
        		<label class="label-home-calc">Срок кредита</label>
        		<input type="text" class="js-range-slider-bank-2" value="" />
        	</div>
        	<div class="extra-controls">
        		<span class="result-title">Ваш ежемесячный <br>платеж составит:</span> <span class="js-result-bank"></span>
        	</div>
        </div>
        <div>
            <div class="range-slider">
                <label class="label-home-calc">Сумма кредита</label>
                <input type="text" class="js-range-slider-mfo-1" value="" />
            </div>
            <div class="range-slider range-slider-year">
                <label class="label-home-calc">Срок кредита</label>
                <input type="text" class="js-range-slider-mfo-2" value="" />
            </div>
            <div class="extra-controls">
                <span class="result-title">Ваш ежемесячный <br>платеж составит:</span> <span class="js-result-mfo"></span>
            </div>
        </div>
    </div>            
</div> 
<div class="form-home-calc">
	<?php echo do_shortcode( '[contact-form-7 id="678" title="Форма калькулятор на главной"]' ); ?>	
</div>