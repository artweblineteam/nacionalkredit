<!-- #special-сonditions -->
	<section id="special-сonditions">
		<div class="container">
			<div class="row">
				<div class="col-md-7">
					<div class="conditions">
						<h2>ВНИМАНИЕ! ОСОБЫЕ УСЛОВИЯ ВЫДАЧИ КРЕДИТА, ТОЛЬКО ДО 30 ОКТЯБРЯ</h2>
						<div class="сond-text">
							успейте оформить кредит <br>без дополнительных расходов на <br>оценку вашей недвижимости
						</div>
						<div class="count-block">
							<span class="count-title">Акция действительна до:</span>
							<div class="countdown">
								<div class="clock" style="margin:2em;"></div>
								<!-- <div class="message"></div> -->
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-1"></div>
				<div class="col-md-4">
					<div class="tablet-form">
						<h5 class="form-title">А МНЕ ДАДУТ КРЕДИТ?</h5>
						<?php echo do_shortcode( '[contact-form-7 id="667" title="А мне дадут кредит?"]' ); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<!-- #special-сonditions END -->