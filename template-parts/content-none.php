<?php
/**
 * Template part for displaying a message that posts cannot be found.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package nacionalkredit
 */

?>

<section class="no-results not-found">
	<div class="container">
		<div class="row">
			<div class="page-header">
					<h1 class="page-title"><?php esc_html_e( 'Кажется, по вашему запросу ничего не найдено :(', 'nacionalkredit' ); ?></h1>
				</div><!-- .page-header -->

				<div class="page-content">
					<p><?php esc_html_e( 'Возможно одна из ссылок ниже Вам будет полезна:', 'nacionalkredit' ); ?></p>

					<?php

						the_widget( 'WP_Widget_Recent_Posts' );

						// Only show the widget if site has multiple categories.
						if ( nacionalkredit_categorized_blog() ) :
					?>

					<?php
						endif;
						
					?>
							</div>
						</div>
				</div><!-- .page-content -->
</section><!-- .no-results -->
