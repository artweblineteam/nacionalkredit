<div class="makeRequest modal-box desctop-views" id="makeRequest" style="display: none;">
	<a href="#" class="js-modal-close close">×</a>
	<div class="callback-form">
		<div class="callback-title">
			<span class="callback-subtitle">ДЛЯ ПОЛУЧЕНИЯ БЫСТРОГО ОТВЕТА ПОЖАЛУЙСТА, ЗАПОЛНИТЕ КОРРЕКТНО ПРЕДЛОЖЕННУЮ НИЖЕ ФОРМУ.</span>
		</div>
		<?php echo do_shortcode( '[contact-form-7 id="679" title="Подать заявку"]' ); ?>
		<span class="note"> *- Мы рассматриваем вопросы только по кредитованию собственников под залог недвижимости в Москве и Московской области.</span>
	</div>
</div>

<div class="makeRequest-button">
	<a href="" class="js-open-modal big-link" data-modal-id="makeRequest" >Подать заявку</a>
</div>