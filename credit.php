<?php
/*
Template Name: Оформить кредит
*/

get_header(); ?>
	<!-- #credit -->
	<section id="get-credit">
		<div class="container">
			<div class="row">
				<h1><span>Подать заявку на кредит</span></h1>
				<p>Если срочно нужны деньги, подать заявку на кредит удобно у нас на сайте в режиме онлайн. Предоставьте нам всю необходимую информацию, заполнив все необходимые поля в предложенной ниже форме. После выполнения необходимых действий, поданная заявка на кредит поступает на обработку к нашим специалистам.</p>
				<div class="makeRequest modal-box desctop-views" id="makeRequest" style="display: none;">
					<a href="#" class="js-modal-close close">×</a>
					<div class="callback-form">
						<div class="callback-title">
							<span class="callback-subtitle">ДЛЯ ПОЛУЧЕНИЯ БЫСТРОГО ОТВЕТА ПОЖАЛУЙСТА, ЗАПОЛНИТЕ КОРРЕКТНО ПРЕДЛОЖЕННУЮ НИЖЕ ФОРМУ.</span>
						</div>
						<?php echo do_shortcode( '[contact-form-7 id="679" title="Подать заявку"]' ); ?>
						<span class="note"> *- Мы рассматриваем вопросы только по кредитованию собственников под залог недвижимости в Москве и Московской области.</span>
					</div>
				</div>

				<div class="makeRequest-button popup-button">
				<a href="" class="js-open-modal popup-link" data-modal-id="makeRequest" >Подать заявку на кредит</a>
				</div>

				<p>Для выяснения всех необходимых деталей и оформления кредита, наши менеджеры свяжутся с Вами по указанным в данной заявке на кредит данным и сообщат Вам предварительное решение. Если у Вас имеются на руках все необходимые документы, то оформление по данной заявке может занять не более одного дня.</p>

				<div class="callback-popup modal-box desctop-views" id="callbackPopup" style="display: none;">
					<a href="#" class="js-modal-close close">×</a>
					<div class="callback-form">
						<div class="callback-title">
							<h3>Вы заказываете обратный звонок.</h3>
							<span class="callback-subtitle">ДАННАЯ УСЛУГА БЕСПЛАТНА.</span>
						</div>
						<?php echo do_shortcode( '[contact-form-7 id="666" title="Контактная форма 1"]' ); ?>
						<span class="note">* - Мы рассматриваем вопросы только по кредитованию собственников под залог недвижимости в Москве и Московской области.</span>
					</div>
				</div>

				<div class="callback-button popup-button">
				<a href="#" class="js-open-modal btn-call-back popup-link" data-modal-id="callbackPopup" >НЕТ ВРЕМЕНИ ЗАПОЛНЯТЬ ЗАЯВКУ? <br>ЗАКАЖИ ОБРАТНЫЙ ЗВОНОК!</a>
				</div>
			</div>
		</div>
	</section>
	<?php get_template_part( 'template-parts/special', 'conditions' ); ?>
	<?php get_template_part( 'template-parts/get', 'loan' ); ?>
	<!-- #credit END-->
<?php get_footer();?>