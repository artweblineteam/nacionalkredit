<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nacionalkredit
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta http-equiv="imagetoolbar" content="no"/>
	<meta http-equiv="msthemecompatible" content="no"/>
	<meta name="HandheldFriendly" content="True"/>
	<meta name="format-detection" content="telephone=no"/>
	<meta name="format-detection" content="address=no"/>
	<meta http-equiv="cleartype" content="on"/>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/flipclock.css">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/calc.css">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/ion.rangeSlider.css">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/ion.rangeSlider.skinFlat.css">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/styles.css">
	
	<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
     <!--[if IE]> 
    	<style>
    		.ie-overlay,.ie-notification{
				display: block !important;
    		}
    	</style> 
    <![endif]-->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<!-- header -->
	<header>
		<div class="top-decorated">
			<div class="container">
				<div class="col-md-4"></div>
				<div class="col-md-4"></div>
				<div class="col-md-4"></div>
			</div>
		</div>
		<div class="block-header">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="logo">
							<a href="<?php echo get_home_url(); ?>" class="home-url">
								<img src="<?php bloginfo('template_directory'); ?>/assets/img/logo.png" alt="Logo Nacional Credit" class="logo-img">
								<div class="logo-text">
									<span class="logo-title">Национал Кредит</span><br>
									<span class="logo-subtitle">Кредиты под залог недвижимости</span>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-4">
						<div class="slogan">
							<span class="slogan-first"> Наши деньги - </span>
							<span class="slogan-second">Ваши мечты</span>
						</div>
					</div>
					<div class="col-md-4">
						<div class="info-block">
							<div class="tel">
								<img src="<?php bloginfo('template_directory'); ?>/assets/img/icon-phone.png" alt="icon-phone">
								<div class="tel-numb">
									<a href="tel:+79646460189" class="number">8 (964) 646 01 89</a>
									<a href="tel:+74994082220" class="number">8 (499) 408 22 20</a>
								</div>
							</div>
							<div class="address">
								<span class="address-title">Приходите:</span>
								<span class="address-description">Москва, Цветной бульвар, д 11, стр. 6</span>
							</div>
						</div>
						<div class="call-back">
							<?php get_template_part('template-parts/call', 'back');?>
						</div>

					</div>
				</div>
				<div class="row menu-row">
					<nav class="nav-bar" id="site-navigation" class="main-navigation nav-bar" role="navigation">
						<div class="decorated-line"></div>
						<div class="menu-bar">
							<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
						</div>

						<div class="mobile-menu mobile-view">
							<div class="icon-menu-wrap">
								<span class="hamburger-icon"></span>				
							</div>
							<nav class="menu-wrap hide">
								<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
							</nav>
						</div>
						<div class="search-block">
							<?php get_search_form();?>
						</div>
						<div class="decorated-arrow"></div>
					</nav>
				</div>
			</div>			
		</div>
	</header>
	<!-- header END -->
<?php //echo "1234"; ?>
