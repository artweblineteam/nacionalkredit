
<div class="ie-overlay"></div>
<div class="ie-notification">
	<h4>Вы используете устаревшую версию браузера</h4>
	<p>Для корректной работы сайта обновите ваш браузер</p>
	<a href="https://www.microsoft.com/en-us/download/internet-explorer.aspx" class="btn"> Oбновить</a>
	<small>Рекомендуемые браузеры: 
		<a href="https://www.google.ru/chrome/browser/desktop/">Google Chrome</a>, 
		<a href="https://www.mozilla.org/ru/firefox/new/">Firefox</a>, 
		<a href="http://www.opera.com/ru/computer/windows">Opera</a>
	</small>
</div>
	<footer class="" role="contentinfo">
 		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-3">
					<?php if ( is_active_sidebar( 'nk_foot_1' ) ) : ?>
					 
						<div id="true-foot-1" class="">
					 
							<?php dynamic_sidebar( 'nk_foot_1' ); ?>
					 
						</div>
					 
					<?php endif; ?>
				</div>
				<div class="col-md-4">
					<?php if ( is_active_sidebar( 'nk_foot_2' ) ) : ?>
					 
						<div id="true-foot-2" class="">
					 
							<?php dynamic_sidebar( 'nk_foot_2' ); ?>
					 
						</div>
					 
					<?php endif; ?>
				</div>
				<div class="col-md-4">
					<?php if ( is_active_sidebar( 'nk_foot_3' ) ) : ?>
					 
						<div id="true-foot-3" class="">
					 
							<?php dynamic_sidebar( 'nk_foot_3' ); ?>
					 
						</div>
					 	
					 	<?php else : ?>

					 	<div class="call-block">
							<div class="info-block">
								<div class="tel">
									<img src="<?php bloginfo('template_directory'); ?>/assets/img/icon-phone.png" alt="icon-phone">
									<div class="tel-numb">
										<a href="tel:+79646460189" class="number">8 (964) 646 01 89</a>
										<a href="tel:+74994082220" class="number">8 (499) 408 22 20</a>
									</div>
								</div>
								<div class="address">
									<span class="address-title">Приходите:</span>
									<span class="address-description">Москва, Цветной бульвар, д 11, стр. 6</span>
								</div>
							</div>
							<div class="call-back">
								<?php get_template_part('template-parts/call', 'back');?>
							</div>
						</div>
						<div class="subfooter">
							<img src="<?php bloginfo('template_directory'); ?>/assets/img/logo.png" alt="logo Nacional Credit">
							<span class="copy">НАЦИОНАЛ КРЕДИТ © 2009 - 2016</span>
						</div>

					<?php endif; ?>
				</div>
			</div>
		</div> 
	</footer>

<?php wp_footer(); ?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/flipclock.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/ion.rangeSlider.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/calc.js"></script>

<script src="<?php bloginfo('template_directory'); ?>/assets/js/app.js"></script>
</body>
	
</html>
