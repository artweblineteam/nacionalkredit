		var clock;
		
		$(document).ready(function() {
			var clock;

			clock = $('.clock').FlipClock({
		        clockFace: 'DailyCounter',
		        showSeconds: false,
		        autoStart: false,
		        callbacks: {
		        	stop: function() {
		        		$('.message').html('The clock has stopped!')
		        	}
		        }
		    });
				    
		    clock.setTime(220880);
		    clock.setCountdown(true);
		    clock.start();

		});

		$(document).ready(function(){
			//----------Validation
			if($('#search').val() !== ""){
		          $('#search').parent().find('label').hide();
		    };

		    if($('#nameFormTablet').val() !== ""){
		          $('#nameFormTablet').parent().parent().find('label').hide();
		    };
		    if($('#telFormTablet').val() !== ""){
		          $('#telFormTablet').parent().parent().find('label').hide();
		    };

			if($('#nameContact').val() !== ""){
		          $('#nameContact').parent().parent().find('label').hide();
		    };
		    if($('#emailContact').val() !== ""){
		          $('#emailContact').parent().parent().find('label').hide();
		    };
		    if($('#subjectContact').val() !== ""){
		          $('#subjectContact').parent().parent().find('label').hide();
		    };
		    if($('#messageContact').val() !== ""){
		          $('#messageContact').parent().parent().find('label').hide();
		    };

		    if($('#nameFormCalc').val() !== ""){
		          $('#nameFormCalc').parent().parent().find('label').hide();
		    };
		    if($('#telFormCalc').val() !== ""){
		          $('#telFormCalc').parent().parent().find('label').hide();
		    };

		    if($('#requestName').val() !== ""){
		          $('#requestName').parent().parent().find('label').hide();
		    };
		    if($('#requestAddress').val() !== ""){
		          $('#requestAddress').parent().parent().find('label').hide();
		    };
		    if($('#requestSelect').val() !== ""){
		          $('#requestSelect').parent().parent().find('label').hide();
		    };
		    if($('#requestNumber').val() !== ""){
		          $('#requestNumber').parent().parent().find('label').hide();
		    };
		    if($('#requestTel').val() !== ""){
		          $('#requestTel').parent().parent().find('label').hide();
		    };
		    if($('#requestEmail').val() !== ""){
		          $('#requestEmail').parent().parent().find('label').hide();
		    };


			function emptyInput() {

		        $("#search").on("focus", function () {
		            $(this).parent().find('label').hide();
		        }).on("blur", function () {
		            if ($(this).val() == "") {
		                $(this).parent().find('label').show();
		            }
		        });
				
				$("#nameFormTablet").on("focus", function () {
		            $(this).parent().parent().find('label').hide();
		        }).on("blur", function () {
		            if ($(this).val() == "") {
		                $(this).parent().parent().find('label').show();
		            }
		        });

		        $("#telFormTablet").on("focus", function () {
		            $(this).parent().parent().find('label').hide();
		        }).on("blur", function () {
		            if ($(this).val() == "") {
		                $(this).parent().parent().find('label').show();
		            }
		        });

		        $("#nameContact").on("focus", function () {
		            $(this).parent().parent().find('label').hide();
		        }).on("blur", function () {
		            if ($(this).val() == "") {
		                $(this).parent().parent().find('label').show();
		            }
		        });

		        $("#emailContact").on("focus", function () {
		            $(this).parent().parent().find('label').hide();
		        }).on("blur", function () {
		            if ($(this).val() == "") {
		                $(this).parent().parent().find('label').show();
		            }
		        });

		        $("#subjectContact").on("focus", function () {
		            $(this).parent().parent().find('label').hide();
		        }).on("blur", function () {
		            if ($(this).val() == "") {
		                $(this).parent().parent().find('label').show();
		            }
		        });

		        $("#messageContact").on("focus", function () {
		            $(this).parent().parent().find('label').hide();
		        }).on("blur", function () {
		            if ($(this).val() == "") {
		                $(this).parent().parent().find('label').show();
		            }
		        });

		        $("#nameFormCalc").on("focus", function () {
		            $(this).parent().parent().find('label').hide();
		        }).on("blur", function () {
		            if ($(this).val() == "") {
		                $(this).parent().parent().find('label').show();
		            }
		        });

		        $("#telFormCalc").on("focus", function () {
		            $(this).parent().parent().find('label').hide();
		        }).on("blur", function () {
		            if ($(this).val() == "") {
		                $(this).parent().parent().find('label').show();
		            }
		        });

		        $("#requestName").on("focus", function () {
		            $(this).parent().parent().find('label').hide();
		        }).on("blur", function () {
		            if ($(this).val() == "") {
		                $(this).parent().parent().find('label').show();
		            }
		        });

		        $("#requestAddress").on("focus", function () {
		            $(this).parent().parent().find('label').hide();
		        }).on("blur", function () {
		            if ($(this).val() == "") {
		                $(this).parent().parent().find('label').show();
		            }
		        });

		        $("#requestSelect").on("focus", function () {
		            $(this).parent().parent().find('label').hide();
		        }).on("blur", function () {
		            if ($(this).val() == "") {
		                $(this).parent().parent().find('label').show();
		            }
		        });

		        $("#requestNumber").on("focus", function () {
		            $(this).parent().parent().find('label').hide();
		        }).on("blur", function () {
		            if ($(this).val() == "") {
		                $(this).parent().parent().find('label').show();
		            }
		        });

		        $("#requestTel").on("focus", function () {
		            $(this).parent().parent().find('label').hide();
		        }).on("blur", function () {
		            if ($(this).val() == "") {
		                $(this).parent().parent().find('label').show();
		            }
		        });

		        $("#requestEmail").on("focus", function () {
		            $(this).parent().parent().find('label').hide();
		        }).on("blur", function () {
		            if ($(this).val() == "") {
		                $(this).parent().parent().find('label').show();
		            }
		        });
		    }
		    emptyInput();
		});
		$(function(){

  var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");

  $('a[data-modal-id]').click(function(e) {
    e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 1);
    //$(".js-modalbox").fadeIn(500);
    var modalBox = $(this).attr('data-modal-id');
    $('#'+modalBox).fadeIn($(this).data());
  });  


  $(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
      $(".modal-overlay").remove();
    });
  });

});


(function($){				
    jQuery.fn.lightTabs = function(options){
        
        var createTabs = function(){
            tabs = this;
            i = 0;
            
            showPage = function(i){
                $(tabs).children("div").children("div").hide();
                $(tabs).children("div").children("div").eq(i).show();
                $(tabs).children("ul").children("li").removeClass("active");
                $(tabs).children("ul").children("li").eq(i).addClass("active");
            }
            
            showPage(0);				
            
            $(tabs).children("ul").children("li").each(function(index, element){
                $(element).attr("data-page", i);
                i++;                        
            });
            
            $(tabs).children("ul").children("li").click(function(){
                showPage(parseInt($(this).attr("data-page")));
            });				
        };		
        return this.each(createTabs);
    };	
})(jQuery);
$(document).ready(function(){
    $(".tabs").lightTabs();
});


$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});

$(document).ready(function(){
  $('.icon-menu-wrap').click(function () {
    $(this).toggleClass('open');
    $('.menu-wrap').toggleClass('hide');
  });
});

$(document).ready(function() {
  if (document.all && !document.addEventListener) {
        $('body').append('<div class="overlayIE"></div>');    
      }
});