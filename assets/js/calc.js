  $( document ).ready(function() {
          $("#credit").submit(function(e){

                e.preventDefault();

                var amount = $("#amount").val();
                var term = $("#term").val();
                var rate = $("#rate").val();
                var startmonth = $("#startmonth").val();
                var startyear = $("#startyear").val();

                $.ajax({
                    type: "POST",
                    url: "calc/ajax-calc.php",
                    data: { amount: amount, term: term, rate: rate, startmonth: startmonth, startyear: startyear}
                })
                    .done(function(json) {
                         var obj = JSON.parse(json);
                         $("#overpay").text(obj.overpay);
                         $("#payment").text(obj.payment);
                         $("#schedule").html(obj.schedule);
                    });
          });
    });

var $rangeBank1 = $(".js-range-slider-bank-1"),
    $rangeBank2 = $(".js-range-slider-bank-2"),
    $resultBank = $(".js-result-bank"),
    v1Bank = 0,
    v2Bank = 0;
    $rangeMfo1 = $(".js-range-slider-mfo-1"),
    $rangeMfo2 = $(".js-range-slider-mfo-2"),
    $resultMfo = $(".js-result-mfo"),
    v1Mfo = 0,
    v2Mfo = 0;

var calcBank = function (data) {
    var pBank = 0.14/12;
    var nBank = v2Bank*12;
    var resBank = v1Bank*(pBank + (pBank/(Math.pow((1+pBank),nBank)-1)));
    
    var resBankToFixed = resBank.toFixed(0);
    var resBankToString = resBankToFixed.toString();

    var outBank = resBankToString.replace(/.+?(?=\D|$)/, function(x) {
        return x.replace(/(\d)(?=(?:\d\d\d)+$)/g, "$1 ");
    });
    $resultBank.html(outBank);
};

var calcMfo = function (data) {
    var pMfo = 0.14/12;
    var nMfo = v2Mfo*12;
    var resMfo = v1Mfo*(pMfo + (pMfo/(Math.pow((1+pMfo),nMfo)-1)));
    
    var resMfoToFixed = resMfo.toFixed(0);
    var resMfoToString = resMfoToFixed.toString();

    var outMfo = resMfoToString.replace(/.+?(?=\D|$)/, function(x) {
        return x.replace(/(\d)(?=(?:\d\d\d)+$)/g, "$1 ");
    });
    $resultMfo.html(outMfo);
};

$rangeBank1.ionRangeSlider({
    type: "single",
    min: 300000,
    max: 5000000,
    from: 1000000,
    grid: true,
    postfix: " руб.",
    onStart: function (data) {
        v1Bank = data.from;
        calcBank();
    },
    onChange: function (data) {
        v1Bank = data.from;
        calcBank();
    },
    onFinish: function (data) {
        v1Bank = data.from;
        calcBank();
    }
});

$rangeBank2.ionRangeSlider({
    type: "single",
    min: 1,
    max: 15,
    from: 6,
    grid: true,
    postfix: " лет",
    onStart: function (data) {
        v2Bank= data.from;
        calcBank();
    },
    onChange: function (data) {
        v2Bank = data.from;
        calcBank();
    },
    onFinish: function (data) {
        v2Bank = data.from;
        calcBank();
    }
});

$rangeMfo1.ionRangeSlider({
    type: "single",
    min: 300000,
    max: 5000000,
    from: 1500000,
    grid: true,
    postfix: " руб.",
    onStart: function (data) {
        v1Mfo = data.from;
        calcBank();
    },
    onChange: function (data) {
        v1Mfo = data.from;
        calcBank();
    },
    onFinish: function (data) {
        v1Mfo = data.from;
        calcBank();
    }
});

$rangeMfo2.ionRangeSlider({
    type: "single",
    min: 1,
    max: 15,
    from: 3,
    grid: true,
    postfix: " лет",
    onStart: function (data) {
        v2Mfo = data.from;
        calcMfo();
    },
    onChange: function (data) {
        v2Mfo = data.from;
        calcMfo();
    },
    onFinish: function (data) {
        v2Mfo = data.from;
        calcMfo();
    }
});