<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package nacionalkredit
 */

get_header(); ?>
	<section id="content" class="site-content">
		<div class="container">
			<div class="row">
				<div id="primary" class="content-area">
					<main id="main" class="site-main" role="main">

					<?php
					while ( have_posts() ) : the_post();

						get_template_part( 'template-parts/content', get_post_format() );

						//the_post_navigation();

					endwhile; // End of the loop.
					?>

					</main><!-- #main -->
				</div><!-- #primary -->
			
			</div>
		</div>
	</section>
	<?php get_template_part( 'template-parts/special', 'conditions' ); ?>
	<?php get_template_part( 'template-parts/get', 'loan' ); ?>
<?php

get_footer();
