<?php
/*
Template Name: Контакты
*/

get_header(); ?>
	<!-- #contact-page -->
<section id="contact-page">
	<div class="container">
		<div class="row">
			<h1><span>Контакты и схема проезда</span></h1>
		</div>
		<div class="row contact-address">
			<div class="col-md-5">
				<h5>Адрес:</h5>
				<span class="address-info">Цветной бульвар, д.11, строение 6, Москва, <br>Московская область,	127051, Россия.</span>
			</div>
			<div class="col-md-3">
				<h5>Телефоны:</h5>
				<span class="address-info">
					<a href="tel:+74994082220"> 8 (499) 408-22-20</a><br>
					<a href="tel:+79646460189"> 8 (964) 646-01-89</a>
				</span>
			</div>
			<div class="col-md-4">
				<h5>E-mail:</h5>
				<span class="address-info">
				<a href="mailto:info@nacionalkredit.ru"> info@nacionalkredit.ru</a>
				</span>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<div class="map">
					<iframe src="https://api-maps.yandex.ua/frame/v1/-/CZg6JF5h" width="100%" height="420" frameborder="0"></iframe>
				</div>
			</div>
			<div class="col-md-4">
				<div class="contact-title">
					<h4>Напишите нам</h4>
					<i>Все поля, отмеченные звёздочкой, являются обязательными для заполнения.</i>
				</div>
				<?php echo do_shortcode( '[contact-form-7 id="670" title="Форма страницы контактов"]' ); ?>
			</div>
		</div>
		<div class="row contact-note">
			<span class="contact-note-message">ВЫ ИЩЕТЕ ФИНАНСОВУЮ КОМПАНИЮ С БЕЗУПРЕЧНОЙ РЕПУТАЦИЕЙ, ЧТОБЫ ОФОРМИТЬ СДЕЛКУ?<br> СПЕЦИАЛИСТЫ КОМПАНИИ 	НАЦИОНАЛ КРЕДИТ ДОРОЖАТ КАЖДЫМ КЛИЕНТОМ И БУДУТ РАДЫ ОТВЕТИТЬ НА ВСЕ ВАШИ ВОПРОСЫ!<br>

				МЫ ВСЕГДА ГОТОВЫ СДЕЛАТЬ ВАМ ВЫГОДНОЕ ПРЕДЛОЖЕНИЕ!</span>
		</div>
	</div>
</section>
<!-- #contact-page END -->
<?php get_footer();?>